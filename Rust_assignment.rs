1a) code:

fn main()
{
    let a=10;
    let b=20;
    add(a,b);
    sub(a,b);
    mul(a,b);
    div(a,b);
}
fn add(a:i32,b:i32)
{
    println!("{}",a+b);
}
fn sub(a:i32,b:i32)
{
    println!("{}",a-b);
}
fn mul(a:i32,b:i32)
{
    println!("{}",a*b);
}
fn div(a:i32,b:i32)
{
    println!("{}",a/b);
}

output: 
30
-10
200
0

1b) code:

fn main()
{
    println!("Multiplication table of 10");
for i in 1..11{
    println!("10*{}={}",i,10*i);
    }
        println!("Addition table of 10");
for i in 1..11{
    println!("10+{}={}",i,10+i);
    }
        println!("Subtraction table of 10");
for i in 1..11{
    println!("10-{}={}",i,10-i);
    }
        println!("Division table of 10");
for i in 1..11{
    println!("10/{}={}",i,10/i);
    }
}

output:
Multiplication table of 10
10*1=10
10*2=20
10*3=30
10*4=40
10*5=50
10*6=60
10*7=70
10*8=80
10*9=90
10*10=100
Addition table of 10
10+1=11
10+2=12
10+3=13
10+4=14
10+5=15
10+6=16
10+7=17
10+8=18
10+9=19
10+10=20
Subtraction table of 10
10-1=9
10-2=8
10-3=7
10-4=6
10-5=5
10-6=4
10-7=3
10-8=2
10-9=1
10-10=0
Division table of 10
10/1=10
10/2=5
10/3=3
10/4=2
10/5=2
10/6=1
10/7=1
10/8=1
10/9=1
10/10=1

1c) code: 

fn main()
{
    println!(" I am Yuktha");
    println!("CSE-3B");
    println!("20b61a0586");
    println!("Nalla Malla Reddy Engineering College");
}

output:
 I am Yuktha
CSE-3B
20b61a0586
Nalla Malla Reddy Engineering College

2a) code:

fn main()
{
    for i in 1..11
    {
        println!("{}",i);
    }
}

output: 
1
2
3
4
5
6
7
8
9
10

2b) code:

fn main() {
    let start = 0.1;
    let end = 1.0;
    let step = 0.1;

    let mut current = start;
    while current <= end {
        println!("{:.1}", current);
        current += step;
    }
}

output:

0.1
0.2
0.3
0.4
0.5
0.6
0.7
0.8
0.9
1.0

2c) code:

fn is_prime(num: u32) -> bool {
    if num < 2 {
        return false;
    }

    for i in 2..(num / 2 + 1) {
        if num % i == 0 {
            return false;
        }
    }

    true
}

fn main() {
    for num in 1..=1000 {
        if num % 2 == 0 {
            println!("{} is even", num);
        } else {
            println!("{} is odd", num);
        }

        if is_prime(num) {
            println!("{} is prime", num);
        }
    }
}

output:
1 is odd
2 is even
2 is prime
3 is odd
3 is prime
4 is even
5 is odd
5 is prime
6 is even
7 is odd
7 is prime
8 is even
9 is odd
10 is even
11 is odd
11 is prime
12 is even
13 is odd
13 is prime
14 is even
15 is odd
16 is even
17 is odd
17 is prime
18 is even
19 is odd
19 is prime
20 is even
21 is odd
22 is even
23 is odd
23 is prime
24 is even
25 is odd
26 is even
27 is odd
28 is even
29 is odd
29 is prime
30 is even
31 is odd
31 is prime
32 is even
33 is odd
34 is even
35 is odd
36 is even
37 is odd
37 is prime
38 is even
39 is odd
40 is even
41 is odd
41 is prime
42 is even
43 is odd
43 is prime
44 is even
45 is odd
46 is even
47 is odd
47 is prime
48 is even
49 is odd
50 is even
51 is odd
52 is even
53 is odd
53 is prime
54 is even
55 is odd
56 is even
57 is odd
58 is even
59 is odd
59 is prime
60 is even
61 is odd
61 is prime
62 is even
63 is odd
64 is even
65 is odd
66 is even
67 is odd
67 is prime
68 is even
69 is odd
70 is even
71 is odd
71 is prime
72 is even
73 is odd
73 is prime
74 is even
75 is odd
76 is even
77 is odd
78 is even
79 is odd
79 is prime
80 is even
81 is odd
82 is even
83 is odd
83 is prime
84 is even
85 is odd
86 is even
87 is odd
88 is even
89 is odd
89 is prime
90 is even
91 is odd
92 is even
93 is odd
94 is even
95 is odd
96 is even
97 is odd
97 is prime
98 is even
99 is odd
100 is even
101 is odd
101 is prime
102 is even
103 is odd
103 is prime
104 is even
105 is odd
106 is even
107 is odd
107 is prime
108 is even
109 is odd
109 is prime
110 is even
111 is odd
112 is even
113 is odd
113 is prime
114 is even
115 is odd
116 is even
117 is odd
118 is even
119 is odd
120 is even
121 is odd
122 is even
123 is odd
124 is even
125 is odd
126 is even
127 is odd
127 is prime
128 is even
129 is odd
130 is even
131 is odd
131 is prime
132 is even
133 is odd
134 is even
135 is odd
136 is even
137 is odd
137 is prime
138 is even
139 is odd
139 is prime
140 is even
141 is odd
142 is even
143 is odd
144 is even
145 is odd
146 is even
147 is odd
148 is even
149 is odd
149 is prime
150 is even
151 is odd
151 is prime
152 is even
153 is odd
154 is even
155 is odd
156 is even
157 is odd
157 is prime
158 is even
159 is odd
160 is even
161 is odd
162 is even
163 is odd
163 is prime
164 is even
165 is odd
166 is even
167 is odd
167 is prime
168 is even
169 is odd
170 is even
171 is odd
172 is even
173 is odd
173 is prime
174 is even
175 is odd
176 is even
177 is odd
178 is even
179 is odd
179 is prime
180 is even
181 is odd
181 is prime
182 is even
183 is odd
184 is even
185 is odd
186 is even
187 is odd
188 is even
189 is odd
190 is even
191 is odd
191 is prime
192 is even
193 is odd
193 is prime
194 is even
195 is odd
196 is even
197 is odd
197 is prime
198 is even
199 is odd
199 is prime
200 is even
201 is odd
202 is even
203 is odd
204 is even
205 is odd
206 is even
207 is odd
208 is even
209 is odd
210 is even
211 is odd
211 is prime
212 is even
213 is odd
214 is even
215 is odd
216 is even
217 is odd
218 is even
219 is odd
220 is even
221 is odd
222 is even
223 is odd
223 is prime
224 is even
225 is odd
226 is even
227 is odd
227 is prime
228 is even
229 is odd
229 is prime
230 is even
231 is odd
232 is even
233 is odd
233 is prime
234 is even
235 is odd
236 is even
237 is odd
238 is even
239 is odd
239 is prime
240 is even
241 is odd
241 is prime
242 is even
243 is odd
244 is even
245 is odd
246 is even
247 is odd
248 is even
249 is odd
250 is even
251 is odd
251 is prime
252 is even
253 is odd
254 is even
255 is odd
256 is even
257 is odd
257 is prime
258 is even
259 is odd
260 is even
261 is odd
262 is even
263 is odd
263 is prime
264 is even
265 is odd
266 is even
267 is odd
268 is even
269 is odd
269 is prime
270 is even
271 is odd
271 is prime
272 is even
273 is odd
274 is even
275 is odd
276 is even
277 is odd
277 is prime
278 is even
279 is odd
280 is even
281 is odd
281 is prime
282 is even
283 is odd
283 is prime
284 is even
285 is odd
286 is even
287 is odd
288 is even
289 is odd
290 is even
291 is odd
292 is even
293 is odd
293 is prime
294 is even
295 is odd
296 is even
297 is odd
298 is even
299 is odd
300 is even
301 is odd
302 is even
303 is odd
304 is even
305 is odd
306 is even
307 is odd
307 is prime
308 is even
309 is odd
310 is even
311 is odd
311 is prime
312 is even
313 is odd
313 is prime
314 is even
315 is odd
316 is even
317 is odd
317 is prime
318 is even
319 is odd
320 is even
321 is odd
322 is even
323 is odd
324 is even
325 is odd
326 is even
327 is odd
328 is even
329 is odd
330 is even
331 is odd
331 is prime
332 is even
333 is odd
334 is even
335 is odd
336 is even
337 is odd
337 is prime
338 is even
339 is odd
340 is even
341 is odd
342 is even
343 is odd
344 is even
345 is odd
346 is even
347 is odd
347 is prime
348 is even
349 is odd
349 is prime
350 is even
351 is odd
352 is even
353 is odd
353 is prime
354 is even
355 is odd
356 is even
357 is odd
358 is even
359 is odd
359 is prime
360 is even
361 is odd
362 is even
363 is odd
364 is even
365 is odd
366 is even
367 is odd
367 is prime
368 is even
369 is odd
370 is even
371 is odd
372 is even
373 is odd
373 is prime
374 is even
375 is odd
376 is even
377 is odd
378 is even
379 is odd
379 is prime
380 is even
381 is odd
382 is even
383 is odd
383 is prime
384 is even
385 is odd
386 is even
387 is odd
388 is even
389 is odd
389 is prime
390 is even
391 is odd
392 is even
393 is odd
394 is even
395 is odd
396 is even
397 is odd
397 is prime
398 is even
399 is odd
400 is even
401 is odd
401 is prime
402 is even
403 is odd
404 is even
405 is odd
406 is even
407 is odd
408 is even
409 is odd
409 is prime
410 is even
411 is odd
412 is even
413 is odd
414 is even
415 is odd
416 is even
417 is odd
418 is even
419 is odd
419 is prime
420 is even
421 is odd
421 is prime
422 is even
423 is odd
424 is even
425 is odd
426 is even
427 is odd
428 is even
429 is odd
430 is even
431 is odd
431 is prime
432 is even
433 is odd
433 is prime
434 is even
435 is odd
436 is even
437 is odd
438 is even
439 is odd
439 is prime
440 is even
441 is odd
442 is even
443 is odd
443 is prime
444 is even
445 is odd
446 is even
447 is odd
448 is even
449 is odd
449 is prime
450 is even
451 is odd
452 is even
453 is odd
454 is even
455 is odd
456 is even
457 is odd
457 is prime
458 is even
459 is odd
460 is even
461 is odd
461 is prime
462 is even
463 is odd
463 is prime
464 is even
465 is odd
466 is even
467 is odd
467 is prime
468 is even
469 is odd
470 is even
471 is odd
472 is even
473 is odd
474 is even
475 is odd
476 is even
477 is odd
478 is even
479 is odd
479 is prime
480 is even
481 is odd
482 is even
483 is odd
484 is even
485 is odd
486 is even
487 is odd
487 is prime
488 is even
489 is odd
490 is even
491 is odd
491 is prime
492 is even
493 is odd
494 is even
495 is odd
496 is even
497 is odd
498 is even
499 is odd
499 is prime
500 is even
501 is odd
502 is even
503 is odd
503 is prime
504 is even
505 is odd
506 is even
507 is odd
508 is even
509 is odd
509 is prime
510 is even
511 is odd
512 is even
513 is odd
514 is even
515 is odd
516 is even
517 is odd
518 is even
519 is odd
520 is even
521 is odd
521 is prime
522 is even
523 is odd
523 is prime
524 is even
525 is odd
526 is even
527 is odd
528 is even
529 is odd
530 is even
531 is odd
532 is even
533 is odd
534 is even
535 is odd
536 is even
537 is odd
538 is even
539 is odd
540 is even
541 is odd541 is prime
542 is even
543 is odd
544 is even
545 is odd
546 is even
547 is odd
547 is prime
548 is even
549 is odd
550 is even
551 is odd
552 is even
553 is odd
554 is even
555 is odd
556 is even
557 is odd
557 is prime
558 is even
559 is odd
560 is even
561 is odd
562 is even
563 is odd
563 is prime
564 is even
565 is odd
566 is even
567 is odd
568 is even
569 is odd
569 is prime
570 is even
571 is odd
571 is prime
572 is even
573 is odd
574 is even
575 is odd
576 is even
577 is odd
577 is prime
578 is even
579 is odd
580 is even
581 is odd
582 is even
583 is odd
584 is even
585 is odd
586 is even
587 is odd
587 is prime
588 is even
589 is odd
590 is even
591 is odd
592 is even
593 is odd
593 is prime
594 is even
595 is odd
596 is even
597 is odd
598 is even
599 is odd
599 is prime
600 is even
601 is odd
601 is prime
602 is even
603 is odd
604 is even
605 is odd
606 is even
607 is odd
607 is prime
608 is even
609 is odd
610 is even
611 is odd
612 is even
613 is odd
613 is prime
614 is even
615 is odd
616 is even
617 is odd
617 is prime
618 is even
619 is odd
619 is prime
620 is even
621 is odd
622 is even
623 is odd
624 is even
625 is odd
626 is even
627 is odd
628 is even
629 is odd
630 is even
631 is odd
631 is prime
632 is even
633 is odd
634 is even
635 is odd
636 is even
637 is odd
638 is even
639 is odd
640 is even
641 is odd
641 is prime
642 is even
643 is odd
643 is prime
644 is even
645 is odd
646 is even
647 is odd
647 is prime
648 is even
649 is odd
650 is even
651 is odd
652 is even
653 is odd
653 is prime
654 is even
655 is odd
656 is even
657 is odd
658 is even
659 is odd
659 is prime
660 is even
661 is odd
661 is prime
662 is even
663 is odd
664 is even
665 is odd
666 is even
667 is odd
668 is even
669 is odd
670 is even
671 is odd
672 is even
673 is odd
673 is prime
674 is even
675 is odd
676 is even
677 is odd
677 is prime
678 is even
679 is odd
680 is even
681 is odd
682 is even
683 is odd
683 is prime
684 is even
685 is odd
686 is even
687 is odd
688 is even
689 is odd
690 is even
691 is odd
691 is prime
692 is even
693 is odd
694 is even
695 is odd
696 is even
697 is odd
698 is even
699 is odd
700 is even
701 is odd
701 is prime
702 is even
703 is odd
704 is even
705 is odd
706 is even
707 is odd
708 is even
709 is odd
709 is prime
710 is even
711 is odd
712 is even
713 is odd
714 is even
715 is odd
716 is even
717 is odd
718 is even
719 is odd
719 is prime
720 is even
721 is odd
722 is even
723 is odd
724 is even
725 is odd
726 is even
727 is odd
727 is prime
728 is even
729 is odd
730 is even
731 is odd
732 is even
733 is odd
733 is prime
734 is even
735 is odd
736 is even
737 is odd
738 is even
739 is odd
739 is prime
740 is even
741 is odd
742 is even
743 is odd
743 is prime
744 is even
745 is odd
746 is even
747 is odd
748 is even
749 is odd
750 is even
751 is odd
751 is prime
752 is even
753 is odd
754 is even
755 is odd
756 is even
757 is odd
757 is prime
758 is even
759 is odd
760 is even
761 is odd
761 is prime
762 is even
763 is odd
764 is even
765 is odd
766 is even
767 is odd
768 is even
769 is odd
769 is prime
770 is even
771 is odd
772 is even
773 is odd
773 is prime
774 is even775 is odd
776 is even
777 is odd
778 is even
779 is odd
780 is even
781 is odd
782 is even
783 is odd
784 is even
785 is odd
786 is even
787 is odd
787 is prime
788 is even
789 is odd
790 is even
791 is odd
792 is even
793 is odd
794 is even
795 is odd
796 is even
797 is odd
797 is prime
798 is even
799 is odd
800 is even
801 is odd
802 is even
803 is odd
804 is even
805 is odd
806 is even
807 is odd
808 is even
809 is odd
809 is prime
810 is even
811 is odd
811 is prime
812 is even
813 is odd
814 is even
815 is odd
816 is even
817 is odd
818 is even
819 is odd
820 is even
821 is odd
821 is prime
822 is even
823 is odd
823 is prime
824 is even
825 is odd
826 is even
827 is odd
827 is prime
828 is even
829 is odd
829 is prime
830 is even
831 is odd
832 is even
833 is odd
834 is even
835 is odd
836 is even
837 is odd
838 is even
839 is odd
839 is prime
840 is even
841 is odd
842 is even
843 is odd
844 is even
845 is odd
846 is even
847 is odd
848 is even
849 is odd
850 is even
851 is odd
852 is even
853 is odd
853 is prime
854 is even
855 is odd
856 is even
857 is odd
857 is prime
858 is even
859 is odd
859 is prime
860 is even
861 is odd
862 is even
863 is odd
863 is prime
864 is even
865 is odd
866 is even
867 is odd
868 is even
869 is odd
870 is even
871 is odd
872 is even
873 is odd
874 is even
875 is odd
876 is even
877 is odd
877 is prime
878 is even
879 is odd
880 is even
881 is odd
881 is prime
882 is even
883 is odd
883 is prime
884 is even
885 is odd
886 is even
887 is odd
887 is prime
888 is even
889 is odd
890 is even
891 is odd
892 is even
893 is odd
894 is even
895 is odd
896 is even
897 is odd
898 is even
899 is odd
900 is even
901 is odd
902 is even
903 is odd
904 is even
905 is odd
906 is even
907 is odd
907 is prime
908 is even
909 is odd
910 is even
911 is odd
911 is prime
912 is even
913 is odd
914 is even
915 is odd
916 is even
917 is odd
918 is even
919 is odd
919 is prime
920 is even
921 is odd
922 is even
923 is odd
924 is even
925 is odd
926 is even
927 is odd
928 is even
929 is odd
929 is prime
930 is even
931 is odd
932 is even
933 is odd
934 is even
935 is odd
936 is even
937 is odd
937 is prime
938 is even
939 is odd
940 is even
941 is odd
941 is prime
942 is even
943 is odd
944 is even
945 is odd
946 is even
947 is odd
947 is prime
948 is even
949 is odd
950 is even
951 is odd
952 is even
953 is odd
953 is prime
954 is even
955 is odd
956 is even
957 is odd
958 is even
959 is odd
960 is even
961 is odd
962 is even
963 is odd
964 is even
965 is odd
966 is even
967 is odd
967 is prime
968 is even
969 is odd
970 is even
971 is odd
971 is prime
972 is even
973 is odd
974 is even
975 is odd
976 is even
977 is odd
977 is prime
978 is even
979 is odd
980 is even
981 is odd
982 is even
983 is odd
983 is prime
984 is even
985 is odd
986 is even
987 is odd
988 is even
989 is odd
990 is even
991 is odd
991 is prime
992 is even
993 is odd
994 is even
995 is odd
996 is even
997 is odd
997 is prime
998 is even
999 is odd
1000 is even





